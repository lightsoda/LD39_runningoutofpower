package;

import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.system.FlxAssets.FlxGraphicAsset;
import motion.Actuate;
import motion.easing.Elastic;
import motion.easing.Linear;

/**
 * ...
 * @author lightsoda
 */
class EBase extends FlxSprite
{

	public var TilePosition:Vec2 = new Vec2();
	public var Offset:FlxPoint = FlxPoint.get();
	public var IsMovingX:Bool = false;
	public var IsMovingY:Bool = false;
	public var Moves:Int = 0;
	public var MaxMoves:Int = 8;
	var updateMoves:Bool = false;
	
	public function new(?X:Float=0, ?Y:Float=0, ?SimpleGraphic:FlxGraphicAsset) 
	{
		super(X, Y, SimpleGraphic);
		
	}
	
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		
		if (!IsMovingX && !IsMovingY) {
			var nx = TilePosition.x * C.TILE_SIZE + Offset.x;
			var ny = TilePosition.y * C.TILE_SIZE + Offset.y;
			
			if (nx != x) {
				IsMovingX = true;
				Actuate.tween(this, C.EASING_TIME, { x: nx }).ease(Linear.easeNone).onComplete(function(){
					IsMovingX = false;
					if (updateMoves) {
						Moves++;
					}
				});
			}
			if (ny != y) {
				IsMovingY = true;
				Actuate.tween(this, C.EASING_TIME, { y: ny }).ease(Linear.easeNone).onComplete(function(){
					IsMovingY = false;
					if (updateMoves) {
						Moves++;
					}
				});
			}
		}
	}
	
}