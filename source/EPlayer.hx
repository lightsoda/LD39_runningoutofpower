package;

import flixel.FlxG;
import flixel.math.FlxMath;
import flixel.system.FlxAssets.FlxGraphicAsset;

/**
 * ...
 * @author lightsoda
 */
class EPlayer extends EBase
{
	public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(AssetPaths.player__png, true, C.TILE_SIZE, C.TILE_SIZE);
		animation.add("idle", [0, 1, 2, 3], C.ANIMATION_FRAME_RATE, true);
		animation.play("idle");
	}
	
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		
		if (!IsMovingX && !IsMovingY) {
			HandleInput();
		}
	}
	
	function HandleInput() 
	{
		updateMoves = false;
		
		if (FlxG.keys.anyJustPressed([W, UP])) {
			TilePosition.y -= 1;
			updateMoves = true;
		}
		if (FlxG.keys.anyJustPressed([D, RIGHT])) {
			TilePosition.x += 1;
			updateMoves = true;
		}
		if (FlxG.keys.anyJustPressed([S, DOWN])) {
			TilePosition.y += 1;
			updateMoves = true;
		}
		if (FlxG.keys.anyJustPressed([A, LEFT])) {
			TilePosition.x -= 1;
			updateMoves = true;
		}
		
		// Keep inside the grid, just hard set these
		//   here since we'll keep them the same
		//   as the generator
		
		if (TilePosition.x < 0) TilePosition.x = 0;
		if (TilePosition.y < 0) TilePosition.y = 0;
		if (TilePosition.x > 7) TilePosition.x = 7;
		if (TilePosition.y > 7) TilePosition.y = 7;
		
	}
	
}