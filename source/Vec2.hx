package;

/**
 * ...
 * @author lightsoda
 */
class Vec2
{
	public var x:Int;
	public var y:Int;
	
	public function new(_x:Int = 0, _y:Int = 0) 
	{
		x = _x;
		y = _y;
	}
	
	public function set(_x:Int, _y:Int) 
	{
		x = _x;
		y = _y;
	}
	
}