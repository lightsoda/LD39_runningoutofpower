package;


/**
 * ...
 * @author lightsoda
 */
class C
{
	static public inline var GAME_WIDTH:Int = 1024;
	static public inline var GAME_HEIGHT:Int = 576;
	
	static public inline var TILE_SIZE:Int = 64;
	
	static public inline var COLOR_BACKGROUND:UInt = 0xff131018;
	
	static public inline var TILE_TYPE_NONE:Int = 0;
	static public inline var TILE_TYPE_SOLID:Int = 1;
	static public inline var TILE_TYPE_EDGE:Int = 5;
	static public inline var TILE_TYPE_STAIRS:Int = 2;
	
	static public inline var ANIMATION_FRAME_RATE:Int = 10;
	
	static public inline var EASING_TIME:Float = 0.05;
}