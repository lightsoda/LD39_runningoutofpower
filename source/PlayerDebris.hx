package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;

/**
 * ...
 * @author lightsoda
 */
class PlayerDebris extends FlxSprite
{

	public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(AssetPaths.playerdebris__png, true, C.TILE_SIZE, C.TILE_SIZE);
		animation.add("idle", [0, 1, 2, 3, 4, 5, 6, 7], C.ANIMATION_FRAME_RATE, false);
		animation.play("idle");
	}
	
}