package;

import flash.display.StageQuality;
import flixel.FlxG;
import flixel.FlxGame;
import openfl.display.Sprite;

class Main extends Sprite
{
	public function new()
	{
		super();
		addChild(new FlxGame(C.GAME_WIDTH, C.GAME_HEIGHT, PlayState, 1, 60, 60, true, false));
		FlxG.stage.quality = StageQuality.LOW;
	}
}
