package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.tile.FlxBaseTilemap.FlxTilemapAutoTiling;
import flixel.tile.FlxTilemap;

class PlayState extends FlxState
{
	var tilemap:FlxTilemap;
	var energyBar:EnergyBar;
	var player:EPlayer;
	var pickups:FlxGroup;
	
	override public function create():Void
	{
		super.create();
		FlxG.camera.bgColor = C.COLOR_BACKGROUND;
		
		tilemap = new FlxTilemap();
		energyBar = new EnergyBar();
		player = new EPlayer();
		
		CreateTilemap();
		
		player.Offset = tilemap.getPosition();
		
		pickups = new FlxGroup();
		
		add(player);
		add(energyBar);
		add(pickups);
		
		Initialize();
	}
	
	function Initialize() {
		FlxG.camera.flash(0x44004466);
		player.Moves = 0;
		player.TilePosition.set(0, 0);
		energyBar.ResetSegments();
		pickups.add(new EPickupBase(tilemap.x + 3 * 64, tilemap.y + 3 * 64));
	}
	
	function CreateTilemap() 
	{
		var tilemapData:TilemapData = new TilemapData();
		tilemapData.Generate();
		
		tilemap.loadMapFrom2DArray(tilemapData.GetData(), AssetPaths.tileset__png, C.TILE_SIZE, C.TILE_SIZE, FlxTilemapAutoTiling.OFF, 0, 0);
		tilemap.screenCenter();
		add(tilemap);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (!player.IsMovingX && !player.IsMovingY) {
			FlxG.overlap(player, pickups, playerCollidePickup);
			energyBar.updateSegments(player.Moves);
		}
		
		if (player.Moves >= player.MaxMoves) {
			add(new PlayerDebris(player.x, player.y));
			Initialize();
		}
	}
	
	function playerCollidePickup(player:FlxObject, pickup:FlxObject) {
		cast(player, EPlayer).Moves = 0;
		pickup.destroy();
	}
}
