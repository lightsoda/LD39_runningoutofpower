package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.math.FlxRect;
import flixel.util.FlxAxes;
import motion.Actuate;

/**
 * ...
 * @author lightsoda
 */
class EnergyBar extends FlxSpriteGroup
{
	var segments:Array<EnergyBarSegment>;
	
	public function new(_segments:Int = 8)
	{
		super();
		
		segments = new Array<EnergyBarSegment>();
		
		for (i in 0..._segments) {
			var s:EnergyBarSegment = new EnergyBarSegment(64 * i);
			segments.push(s);
			s.alpha = 0;
			Actuate.tween(s, C.EASING_TIME, { alpha: 1 }).delay(i * 0.1);
			add(s);
		}
		
		screenCenter(FlxAxes.X);
		y = FlxG.height - C.TILE_SIZE;
	}

	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
	}
	
	public function updateSegments(moves:Int) 
	{
		var visibleSegments:Int = 8 - moves;
		
		for (index in 0...visibleSegments) {
			if (!segments[index].alive) {
				resetSegment(segments[index]);
			}
		}
		
		for (index in visibleSegments...segments.length) {
			if (segments[index].alive) {
				segments[index].snuff();
			}
		}
	}
	
	public function ResetSegments() 
	{
		var i:Int = 0;
		for (s in segments) {
			resetSegment(s, i);
			i++;
		}
	}
	
	function resetSegment(s:EnergyBarSegment, i:Int = 0) 
	{
		s.reset(s.x, s.y);
		s.animation.play("idle", false, false, FlxG.random.int(0, 3));
		s.alpha = 0;
		Actuate.tween(s, C.EASING_TIME, { alpha: 1 }).delay(i * 0.1);
	}
	
}