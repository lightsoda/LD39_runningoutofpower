package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;

/**
 * ...
 * @author lightsoda
 */
class EnergyBarSegment extends FlxSprite
{

	public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(AssetPaths.energybar__png, true, C.TILE_SIZE, C.TILE_SIZE);
		animation.add("idle", [0, 1, 2, 3], C.ANIMATION_FRAME_RATE);
		animation.add("kill", [4, 5, 6, 7], C.ANIMATION_FRAME_RATE, false);
		animation.play("idle", false, false, FlxG.random.int(0, 3));
	}
	
	public function snuff():Void 
	{
		animation.play("kill");
		animation.finishCallback = function(name){
			kill();
		}
	}
}