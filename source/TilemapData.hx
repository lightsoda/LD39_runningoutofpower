package;

/**
 * ...
 * @author lightsoda
 */
class TilemapData
{
	var _data:Array<Int> = [];
	
	public var Width:Int = 8;
	public var Height:Int = 9;
	
	public function new() 
	{
		_data = [];
		
		for (x in 0...Width) {
			for (y in 0...Height) {
				SetValue(x, y, C.TILE_TYPE_NONE);
			}
		}
		trace(_data.length);
	}
	
	public function GetData() :Array<Array<Int>>
	{
		var ret:Array<Array<Int>> = new Array<Array<Int>>();
		
		var i:Int = 0;
		for (y in 0...Height) {
			var start:Int = Width * i;
			ret.push(_data.slice(start, start + Width));
			i++;
		}
		
		return ret;
	}
	
	public function Generate() 
	{
		for (x in 0...Width) {
			for (y in 0...Height) {
				SetValue(x, y, C.TILE_TYPE_SOLID);
			}
		}
		
		for (xx in 0...Width) {
			var yy:Int = 8;
			SetValue(xx, yy, C.TILE_TYPE_NONE);
		}
		
		SetValue(1, 1, C.TILE_TYPE_NONE);
		SetValue(Width - 2, 1, C.TILE_TYPE_NONE);
		SetValue(Width - 2, Height - 3, C.TILE_TYPE_NONE);
		SetValue(1, Height - 3, C.TILE_TYPE_NONE);

		SetValue(0, 0, C.TILE_TYPE_STAIRS);
		SetValue(Width - 2, Height - 2, C.TILE_TYPE_STAIRS);
		
		FindWalls();
	}
	
	function FindWalls() 
	{
		for (x in 0...Width) {
			for (y in 0...Height) {
				var val:Int = GetValue(x, y);
				
				if (val == C.TILE_TYPE_NONE) {
					if (GetValue(x, y - 1) == C.TILE_TYPE_SOLID) {
						SetValue(x, y, C.TILE_TYPE_EDGE);
					}
				}
			}
		}
	}
	
	function SetValue(x:Int, y:Int, value:Int) 
	{
		_data[x + Width * y] = value;
	}
	
	function GetValue(x:Int, y:Int):Int
	{
		return _data[x + Width * y];
	}
	
}